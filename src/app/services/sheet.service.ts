import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Sheet } from '../models/sheet';


@Injectable({
  providedIn: 'root'
})
export class SheetService {

  sheetsList: AngularFireList<any>;
  selectedSheet: Sheet = new Sheet();

  constructor(private firebase: AngularFireDatabase) { }

  getSheets() {
    return this.sheetsList = this.firebase.list('sheet');
  }

  insertSheet(sheet: Sheet) {
    this.sheetsList.push(
      // JSON.stringify(sheet)
      {
      fecha: sheet.fecha,
      Jira: sheet.Jira,
      Cliente: sheet.Cliente,
      Angular: sheet.Angular,
      Ionic: sheet.Ionic
    }
  );
  }

  updateSheet(sheet: Sheet) {
    this.sheetsList.update(sheet.$key, {
      fecha: sheet.fecha,
      Jira: sheet.Jira,
      Cliente: sheet.Cliente,
      Angular: sheet.Angular,
      Ionic: sheet.Ionic
    });
  }

  deleteSheet(sheet: Sheet) {
    this.sheetsList.remove(sheet.$key);
  }
}
