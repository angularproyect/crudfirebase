import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

// Service
import { SheetService } from '../../../services/sheet.service';

// SheetClass
import { Sheet } from '../../../models/sheet';
import { ToastrService } from '../../../../../node_modules/ngx-toastr';

@Component({
  selector: 'app-sheet',
  templateUrl: './sheet.component.html',
  styleUrls: ['./sheet.component.css']
})
export class SheetComponent implements OnInit {

  constructor(public sheetService: SheetService, public toastr: ToastrService) { }

  ngOnInit() {
    this.sheetService.getSheets();
    this.resetForm();
  }

  onSubmit(sheetForm: NgForm) {
    // console.log(sheetForm.value);
    if (sheetForm.value.$key == null) {
      this.sheetService.insertSheet(sheetForm.value);
      this.toastr.success('Insert', 'New Data');
    } else {
      this.sheetService.updateSheet(sheetForm.value);
      this.toastr.success('Update', 'Update Data');
    }
    this.resetForm(sheetForm);
  }

  resetForm(sheetForm?: NgForm) {
    if (sheetForm != null) {
      sheetForm.reset();
      this.sheetService.selectedSheet = new Sheet ();
    }
  }
}
